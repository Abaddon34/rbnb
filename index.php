<?php
require_once("App\Defines\Define.php");

use Core\View;
use \App\Controllers\WelcomeController;
use \App\Controllers\AccueilController;
use \App\Controllers\VendeurController;
use \App\Controllers\AcheteurController;
use \App\Controllers\MultiController;

spl_autoload_register();

session_start();

require_once 'vendor' . DS . 'autoload.php';

$router = new \MiladRahimi\PhpRouter\Router();

if(isset($_SESSION['user']))
{
	$router
		->get( '/ChangeRole', MultiController::class . '@ChangeRole' );
		
	switch ($_SESSION['user']->role) 
	{
		case 0:
			$router
				->get( '/Reservations', AcheteurController::class . '@Reservations' )
				->get( '/Reserve', AcheteurController::class . '@Reserve' )
				->post( '/Reserve', AcheteurController::class . '@ReserveAction' );
			break;
		case 1:
			$router
				->get( '/Annonces', VendeurController::class . '@Annonces' )
				->get( '/AjoutAnnonces', VendeurController::class . '@AjoutAnnonces' )
				->post( '/AjoutAnnonces', VendeurController::class . '@AjoutAnnoncesAction' )
				->post( '/AjoutAnnoncesImage', VendeurController::class . '@AjoutAnnoncesImage' )
				->get( '/SupressionAnnonce', VendeurController::class . '@SupressionAnnonce' )
				->get( '/Reservations', VendeurController::class . '@Reservations' );
			break;	
	}
}
else
{
	$router
		->get( '/Connexion', WelcomeController::class . '@Connexion' )
		->post( '/Connexion', WelcomeController::class . '@ConnexionAction' )
		->get( '/Inscription', WelcomeController::class . '@Inscription' )
		->post( '/Inscription', WelcomeController::class . '@InscriptionAction' );	
}
$router
	->get( '/Detailles', AccueilController::class . '@Detailles' )
	->get( '/Accueil', AccueilController::class . '@Accueil' )
	->get( '/Mentions', AccueilController::class . '@Mentions' )
	->get( '/', WelcomeController::class . '@Welcome' );


// Démarrage du routeur
try 
{
	$router->dispatch();
}
catch( \MiladRahimi\PhpRouter\Exceptions\RouteNotFoundException $exception )
{
	View::getError(1); // Erreur 404 page non trouvé
}
catch( \MiladRahimi\PhpRouter\Exceptions\InvalidControllerException $exception ) 
{
	View::getError(2); // Erreur sur le controller
}