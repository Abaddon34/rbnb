
CREATE TABLE IF NOT EXISTS users
(
	id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	login VARCHAR(65) NOT NULL,
	password VARCHAR(255) NOT NULL,
	role INT(1) NOT NULL,
	UNIQUE(login)
);

CREATE TABLE IF NOT EXISTS annonces
(
	id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	author_id INT(10) UNSIGNED NOT NULL,
	couchage INT(10) NOT NULL,
	type VARCHAR(65) NOT NULL,
	taille INT(10) NOT NULL,
	image VARCHAR(65) NOT NULL,
	pays VARCHAR(65) NOT NULL,
	ville VARCHAR(65) NOT NULL,
	adresse VARCHAR(65),
	disponible_start DATETIME NOT NULL,
	disponible_end DATETIME NOT NULL,
	price int(10) NOT NULL,
	description TEXT,
	FOREIGN KEY(author_id) REFERENCES users(id)
);

INSERT INTO annonces
VALUES (1, 1, 5, "Logement entier", 50,'Photos_Annonces_2.jpg', "France", "Nimes", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 26, 'Beau bien dans la ville de france, vue sur le jardin de la fontaine');

INSERT INTO annonces
VALUES (2, 2, 4, "Logement entier", 75,'Photos_Annonces_2.jpg', "France", "Monptellier", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 49, 'Ville vraiment pas belle mais en vrai ca passe parce qu on kiff tous');

INSERT INTO annonces
VALUES (3, 3, 3, "Chambre privé", 125,'Photos_Annonces_3.jpg', "France", "Paris", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 30, 'Ville magnifique vue sur les champs paysage, magnifique ! venez vite !');

INSERT INTO annonces
VALUES (4, 4, 3, "Chambre partager", 80,'Photos_Annonces_3.jpg', "Etats Unis", "New York", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 50.00, 'Ville vraiment pas belle mais en vrai ca passe parce qu on kiff tous');

INSERT INTO annonces
VALUES (5, 5, 3, "Logement entier", 85,'Photos_Annonces_2.jpg', "Angleterre", "Londre", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 45, 'Beau bien dans la ville de france, vue sur le jardin de la fontaine');

INSERT INTO annonces
VALUES (6, 2, 3, "Chambre partager", 92,'Photos_Annonces_3.jpg', "Japon", "Tokyo", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 89, "Ville plutot sympathique, beaucoups de commerces a proximité, la plage a porté de main !");

INSERT INTO annonces
VALUES (7, 3, 3, "Logement entier", 48,'Photos_Annonces_2.jpg', "Espagne", "Madrid", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 27, "Ville plutot sympathique, beaucoups de commerces a proximité, la plage a porté de main !");

INSERT INTO annonces
VALUES (8, 4, 3, "Chambre privé", 74,'Photos_Annonces_3.jpg', "Australie", "Sidney", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 78, 'Beau bien dans la ville de france, vue sur le jardin de la fontaine');

INSERT INTO annonces
VALUES (9, 5, 3, "Chambre partager", 78,'Photos_Annonces_3.jpg', "France", "Marseille", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 225, 'Ville magnifique vue sur les champs paysage, magnifique ! venez vite !');

INSERT INTO annonces
VALUES (10, 6, 3, "Logement entier", 129,'Photos_Annonces_2.jpg', "Espagne", "Barcelone", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 65, 'Ville magnifique vue sur les champs paysage, magnifique ! venez vite !');

INSERT INTO annonces
VALUES (11, 1, 3, "Chambre privé", 136,'Photos_Annonces_3.jpg', "Venezuela", "Caracas", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 45, 'Ville vraiment pas belle mais en vrai ca passe parce qu on kiff tous');

INSERT INTO annonces
VALUES (12, 2, 3, "Logement entier", 18,'Photos_Annonces_2.jpg', "France", "Perpignan", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 96, 'Beau bien dans la ville de france, vue sur le jardin de la fontaine');

INSERT INTO annonces
VALUES (13, 3, 3, "Logement entier", 145,'Photos_Annonces_3.jpg', "France", "Le Soler", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 26, 'Ville vraiment pas belle mais en vrai ca passe parce qu on kiff tous');

INSERT INTO annonces
VALUES (14, 1, 3, "Chambre partager", 69,'Photos_Annonces_3.jpg', "France", "Le Mans", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 78, 'Ville vraiment pas belle mais en vrai ca passe parce qu on kiff tous');

INSERT INTO annonces
VALUES (15, 2, 3, "Chambre privé", 66,'Photos_Annonces_3.jpg', "France", "Cholet", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 59, 'Beau bien dans la ville de france, vue sur le jardin de la fontaine');

INSERT INTO annonces
VALUES (16, 4, 3, "Logement entier", 56,'Photos_Annonces_2.jpg', "Chine", "Pekin", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 155, "Ville plutot sympathique, beaucoups de commerces a proximité, la plage a porté de main !");

INSERT INTO annonces
VALUES (17, 5, 3, "Chambre partager", 41,'Photos_Annonces_3.jpg', "Thailande", "Bangkok", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 252, "Endroit lugubre mais parfait pour les fan d'horreur !");

INSERT INTO annonces
VALUES (18, 6, 3, "Chambre privé", 91,'Photos_Annonces_3.jpg', "Tunisie", "Tunis", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 45, 'Beau bien dans la ville de france, vue sur le jardin de la fontaine');

INSERT INTO annonces
VALUES (19, 5, 3, "Logement entier", 93,'Photos_Annonces_3.jpg', "Chili", "Santiago", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 210, 'Beau bien dans la ville de france, vue sur le jardin de la fontaine');

INSERT INTO annonces
VALUES (20, 6, 3, "Chambre partager", 99,'Photos_Annonces_3.jpg', "France", "Narbonne", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 845, "Ville plutot sympathique, beaucoups de commerces a proximité, la plage a porté de main !");

INSERT INTO annonces
VALUES (21, 1, 3, "Logement entier", 100,'Photos_Annonces_2.jpg', "France", "Brest", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 45, "Endroit lugubre mais parfait pour les fan d'horreur !");

INSERT INTO annonces
VALUES (22, 4, 3, "Chambre partager", 105,'Photos_Annonces_3.jpg', "Egypte", "Le cair", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 20, , 'Beau bien dans la ville de france, vue sur le jardin de la fontaine');

INSERT INTO annonces
VALUES (23, 6, 3, "Chambre privé", 89,'Photos_Annonces_3.jpg', "Senegale", "Dakar", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 80, "Ville plutot sympathique, beaucoups de commerces a proximité, la plage a porté de main !");

INSERT INTO annonces
VALUES (24, 4, 3, "Logement entier", 87,'Photos_Annonces_3.jpg', "Grece", "Athenes", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 123, "Endroit lugubre mais parfait pour les fan d'horreur !");

INSERT INTO annonces
VALUES (25, 2, 3, "Chambre partager", 45,'Photos_Annonces_3.jpg', "Siri", "Damas", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 65, 'Beau bien dans la ville de france, vue sur le jardin de la fontaine');

INSERT INTO annonces
VALUES (26, 3, 3, "Chambre privé", 126,'Photos_Annonces_2.jpg', "Cuba", "Havane", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 75, "Ville plutot sympathique, beaucoups de commerces a proximité, la plage a porté de main !");

INSERT INTO annonces
VALUES (27, 3, 3, "Chambre partager", 78,'Photos_Annonces_3.jpg', "Afghanistan", "kaboule", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 45, "Endroit lugubre mais parfait pour les fan d'horreur !");

INSERT INTO annonces
VALUES (28, 1, 3, "Logement entier", 69,'Photos_Annonces_3.jpg', "Koweit", "Koweit", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 28, "Endroit lugubre mais parfait pour les fan d'horreur !");

INSERT INTO annonces
VALUES (29, 2, 3, "Chambre privé", 78,'Photos_Annonces_2.jpg', "Mexique", "Mexico", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 85, "Ville plutot sympathique, beaucoups de commerces a proximité, la plage a porté de main !");

INSERT INTO annonces
VALUES (30, 3, 3, "Chambre privé", 104,'Photos_Annonces_3.jpg', "Kenya", "Nairobi", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 94, 'Beau bien dans la ville de france, vue sur le jardin de la fontaine');

INSERT INTO annonces
VALUES (31, 4, 3, "Chambre privé", 102,'Photos_Annonces_3.jpg', "Suisse", "Bern", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 45, "Ville plutot sympathique, beaucoups de commerces a proximité, la plage a porté de main !";

INSERT INTO annonces
VALUES (32, 5, 3, "Logement entier", 77,'Photos_Annonces_3.jpg', "Bolivie", "La paz", "866 chemin des terres de rouviere", "2020-09-01T01:01", "2020-10-01T01:01", 78, 'Beau bien dans la ville de france, vue sur le jardin de la fontaine');



CREATE TABLE IF NOT EXISTS reservations
(
	id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	reservor_id INT(10) UNSIGNED NOT NULL,
	annonce_id INT(10) UNSIGNED NOT NULL,
	reservation_start DATETIME NOT NULL,
	reservation_end DATETIME NOT NULL,
	FOREIGN KEY(reservor_id) REFERENCES users(id),
	FOREIGN KEY(annonce_id) REFERENCES annonces(id)
);

CREATE TABLE IF NOT EXISTS liaison
(
	annonce_id INT(10) UNSIGNED NOT NULL,
	equipement_id INT(10) UNSIGNED NOT NULL,
	FOREIGN KEY(annonce_id) REFERENCES annonces(id),
	FOREIGN KEY(equipement_id) REFERENCES equipements(id)
);


CREATE TABLE IF NOT EXISTS equipements
(
	id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	equipement VARCHAR(65) NOT NULL
);

select * from reservations left join annonces on annonces.users_id = $data

select * from reservations where acheteur_id = $data