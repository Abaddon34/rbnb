<?php

namespace Core;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class View
{
	private const VIEW_PATH = ROOT_PATH . 'App' . DS . 'Views' . DS;

	private string $_path_file;

	private bool $_view_file_exists = false;
	public function getViewFileExists(): bool { return $this->_view_file_exists; }

	public static function getError(int $errorCode): void
	{
		$view = new View(''.$errorCode.'error.html.twig');
		$view->renderTwig([ 
			 
		]);
	}

	public function __construct( string $view_name )
	{
		$this->_path_file = $view_name;

		// Test de l'existance du fichier
		$this->_view_file_exists = is_readable( $this->_path_file );
	}

	public function renderTwig( array $view_data = [] ): void
	{
		$loader = new FilesystemLoader(['App\Views\Error', 'App\Views\BaseTemplate', 'App\Views\WelcomeController', 'App\Views\AccueilController', 'App\Views\VendeurController', 'App\Views\AcheteurController']);
    	$twig = new Environment($loader);
    	echo $twig->render($this->_path_file, $view_data);
	}
}