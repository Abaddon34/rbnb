<?php

namespace Core;

use \PDO;

class Database
{
	private static ?PDO $instance = null;

	private static $DSN = 'mysql:host=localhost;dbname=RBNB';
	private static $DB_USER = 'root';
	private static $DB_PASS = 'Faucheuse34';
	private static $PDO_OPTIONS = 
	[
		PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
		PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
		PDO::ATTR_PERSISTENT => true,
		PDO::ATTR_EMULATE_PREPARES => false
	];

	public static function get(): PDO
	{
		if(is_null( self::$instance)) 
		{
			self::$instance = new PDO(self::$DSN, self::$DB_USER, self::$DB_PASS, self::$PDO_OPTIONS );
		}

		return self::$instance;
	}

	private function __construct() { }
	private function __clone() { }
	private function __wakeup() { }
}