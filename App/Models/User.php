<?php

namespace App\Models;

use Core\Model;

class User extends Model
{
	public string $login;
	public string $password;
	public int $role;
}