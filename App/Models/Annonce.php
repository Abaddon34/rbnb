<?php

namespace App\Models;

use Core\Model;

class Annonce extends Model
{
	public int $author_id;
	public int $couchage;
	public string $type;
	public int $taille;
	public string $image;
	public string $pays;
	public string $ville;
	public string $adresse;
	public string $disponible_start;
	public string $disponible_end;
	public int $price;
	public string $description;
}