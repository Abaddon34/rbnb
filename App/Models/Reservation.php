<?php

namespace App\Models;

use Core\Model;

class Reservation extends Model
{
	public int $reservor_id;
	public int $annonce_id;
	public string $reservation_start;
	public string $reservation_end;
}