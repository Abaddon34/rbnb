<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\Liaison;

class LiaisonRepository extends Repository
{
	public function getTable(): string
	{
		return 'liaison';
	}

	public function findByLiaison( string $datatoget, string $data, string $othertable ): ?Liaison
	{
		return $this->readByLiaison( $datatoget, $data, $othertable, Liaison::class );
	}

	public function findBySomething( string $datatoget, string $data): ?Liaison
	{
		return $this->readBySomething( $datatoget, $data, 	Liaison::class );
	}

	public function createLiaison( Liaison $object ): ?Liaison
	{
		return $this->create( $object, Model::class );
	}
	public function findAnLiaison(): ?Liaison
	{
		return $this->readAnLiaison( Liaison::class );
	}
}