<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\Annonce;

class AnnonceRepository extends Repository
{
	public function getTable(): string
	{
		return 'annonces';
	}

	public function findBySome( int $p, array $columns ): ?array
	{
		return $this->readBySome( $p, $columns, Annonce::class );
	}

	public function Count(): ?int
	{
		return $this->CountAll();
	}

	public function findBySomething(string $datatoget, string $data): ?Annonce
	{
		return $this->readBySomething( $datatoget, $data, Annonce::class );
	}

	public function findEquip(string $datatoget, string $data): ?Annonce
	{
		return $this->readEquip( $datatoget, $data, Annonce::class );
	}

	public function findByAll(string $data, string $datatoget, array $columns , string $classname ): ?array
	{
		return $this->readByAll( $data, $datatoget, $columns, Annonce::class );
	}

	public function deleted( int $id )
	{
		return $this->delete( $id, Annonce::class);
	}

	public function createAnnonce( Annonce $object ): ?Annonce
	{
		return $this->create( $object, Model::class );
	}
}