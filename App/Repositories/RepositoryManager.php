<?php

namespace App\Repositories;

use Core\Database;

class RepositoryManager
{
	private static ?self $instance = null;

	private UserRepository $user_repo;
	public function GetUserRepo(): UserRepository
	{
		return $this->user_repo;
	}

	private AnnonceRepository $annonce_repo;
	public function GetAnnonceRepo(): AnnonceRepository
	{
		return $this->annonce_repo;
	}

	private ReservationRepository $reservation_repo;
	public function GetReservationRepo(): ReservationRepository
	{
		return $this->reservation_repo;
	}

	private LiaisonRepository $liaison_repo;
	public function GetLiaisonRepo(): LiaisonRepository
	{
		return $this->liaison_repo;
	}

	public static function getRm(): self
	{
		if( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	private function __construct()
	{
		$pdo = Database::get();

		$this->user_repo = new UserRepository( $pdo );
		$this->annonce_repo = new AnnonceRepository( $pdo );
		$this->reservation_repo = new ReservationRepository( $pdo );
		$this->liaison_repo = new LiaisonRepository( $pdo );
	}

	private function __clone() { }
	private function __wakeup() { }
}