<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\Reservation;

class ReservationRepository extends Repository
{
	public function getTable(): string
	{
		return 'reservations';
	}

	public function findByLiaison( string $datatoget, string $data, string $othertable ): ?Reservation
	{
		return $this->readByLiaison( $datatoget, $data, $othertable, Reservation::class );
	}

	public function findBySomething( string $datatoget, string $data): ?Reservation
	{
		return $this->readBySomething( $datatoget, $data, 	Reservation::class );
	}

	public function createReservation( Reservation $object ): ?Reservation
	{
		return $this->create( $object, Model::class );
	}
}