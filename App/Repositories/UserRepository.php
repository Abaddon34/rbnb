<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\User;

class UserRepository extends Repository
{
	public function getTable(): string
	{
		return 'users';
	}

	public function findBySomething(string $datatoget, string $data ): ?User
	{
		return $this->readBySomething( $datatoget, $data, User::class );
	}

	public function createUser( User $object ): ?User
	{
		return $this->create( $object, Model::class );
	}
}