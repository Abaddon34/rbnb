<?php

namespace App\Controllers;

use App\Models\Annonce;
use Core\Controller;
use Core\View;
use App\Repositories\RepositoryManager;

class AccueilController extends Controller
{
	public function Accueil(): void
	{
		if(isset($_GET['p']))
		{
			$p = $_GET['p'];
		}
		else
		{
			$p = 0;
		}
		$columns = 
		[
			'id',
			'couchage',
			'type',
			'taille',
			'image',
			'pays',
			'ville',
			'price',
			'description'
		];

		if (isset($_SESSION['user']->role))
		{
			switch ($_SESSION['user']->role) 
			{
			case 0:
				$view = new view('accueil_acheteur.html.twig');
				break;
			case 1:
				$view = new view('accueil_vendeur.html.twig');
				break;
			default:
				View::getError(5); // erreur role invalide
				break;
			}
		}
		else
		{
			$view = new view('accueil.html.twig');
		}
		$page = floor($page = $this->rm->getAnnonceRepo()->Count()/ITEMS_PER_PAGE);
		$view->renderTwig([ 
			'annonces' => $this->rm->getAnnonceRepo()->findBySome($p, $columns),
			'pages' => $page
		]);
	}

	public function Detailles(): void
	{
		$datatoget = "id";

		if(isset($_GET['id']))
		{
			$data = $_GET['id'];
		}
		else
		{
			View::getError(6); // erreur page interdite d'acces sans passer par un lien
		}

		if(isset($_SESSION['user']->role))
		{
			switch ($_SESSION['user']->role) 
			{
			case 0:
				$view = new view('Detailles_acheteur.html.twig');
				break;
			case 1:
				$view = new view('Detailles.html.twig');
				break;
			}
		}
		else
		{

			$view = new view('Detailles.html.twig');
		}
		$view->renderTwig([ 
			'annonces' => $this->rm->getAnnonceRepo()->findBySomething($datatoget, $data),
			'equipements' => $this->rm->getLiaisonRepo()->findAnLiaison()
		]);
	}

	public function Mentions(): void
	{
		$view = new View('mentions.html.twig');
		$view->renderTwig([ 
			
		]);
	}
}