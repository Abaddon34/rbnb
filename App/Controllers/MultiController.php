<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;
use App\Repositories\RepositoryManager;

class MultiController extends Controller
{
	public function ChangeRole(): void
	{
		if(isset($_SESSION['user']->role))
		{
			switch ($_SESSION['user']->role) 
			{
			case 0:
				$_SESSION['user']->role = 1;
				header('location: Accueil');
				break;
			case 1:
				$_SESSION['user']->role = 0;
				header('location: Accueil');
				break;
			default:
				View::getError(5); // erreur role invalide
				break;
			}
		}
		else
		{
			View::getError(5);
		}
	}
}