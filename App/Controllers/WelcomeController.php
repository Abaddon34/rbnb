<?php

namespace App\Controllers;

use App\Models\User;
use Core\Controller;
use Core\View;
use App\Repositories\RepositoryManager;

class WelcomeController extends Controller
{
	public function Welcome(): void
	{
		session_destroy();
		$view = new View('welcome.html.twig');
		$view->renderTwig([ 

		]);
	}

	public function Connexion(): void
	{
		session_destroy();
		$view = new View('connexion.html.twig');
		$view->renderTwig([ 
			
		]);
	}

	public function ConnexionAction(): void
	{
		if(!empty($_POST['Login']) && !preg_match('/^[a-z0-9_]+$/', $_POST['Login']) && !empty($_POST['Password']))
		{
			$datatoget = "login";

			$data = $_POST['Login'];

			$user = $this->rm->getUserRepo()->findBySomething($datatoget, $data );

			if(password_verify($_POST['Password'], $user->password))
			{
				$user->password = "";

				$_SESSION['user'] = $user;

				$view = new View('reussite_connexion.html.twig');
				$view->renderTwig([ 

				]);
			}
			else
			{
				View::getError(4);
			}
		}
		else
		{
			View::getError(3);
		}
	}

	public function Inscription(): void
	{
		session_destroy();
		$view = new View('inscription.html.twig');
		$view->renderTwig([ 
			
		]);
	}

	public function InscriptionAction(): void
	{
		if(!empty($_POST['Login']) && !preg_match('/^[a-z0-9_]+$/', $_POST['Login']) && !empty($_POST['Password']) && ($_POST['Choix_Role'] == 1 || $_POST['Choix_Role'] == 0))
 		{
 			$password = password_hash($_POST['Password'], PASSWORD_ARGON2ID);

 			$request = new User();
 			$request->login = $_POST['Login'];
 			$request->password = $password;
 			$request->role = $_POST['Choix_Role'];
			$user = $this->rm->getUserRepo()->createUser($request);
			
			$user->password = "";

			$_SESSION['user'] = $user;

			$view = new View('reussite_inscription.html.twig');
			$view->renderTwig([ 

				]);
 		}
 		else
 		{
 			View::getError(3);
 		}
 	}
}