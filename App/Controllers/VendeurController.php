<?php

namespace App\Controllers;

use App\Models\Reservation; 
use App\Models\Annonce; 
use Core\Controller;
use Core\View;
use App\Repositories\RepositoryManager;

class VendeurController extends Controller
{
	public function Annonces(): void
	{
		require_once("App\Defines\Verification.php");

		$view = new View('annonces.html.twig');

		$columns = 
		[
			'id',
			'couchage',
			'type',
			'taille',
			'image',
			'pays',
			'ville',
			'price',
			'description'
		];

		$data = 'annonces.author_id';
		$datatoget = $_SESSION['user']->role;
		$classnamefromwho = 'users';

		$view->renderTwig([ 
			'annonces' => $this->rm->getAnnonceRepo()->findByAll($data, $datatoget, $columns, $classnamefromwho)
		]);
	}

	public function AjoutAnnonces(): void
	{
		require_once("App\Defines\Verification.php");

		$view = new View('ajoutannoncesimage.html.twig');

		$view->renderTwig([ 
			
		]);
	}

	public function AjoutAnnoncesAction(): void
	{
		if(!empty($_POST['Pays']))// && !preg_match('/^[A-Za-z\d ]{1,50}$/', $_POST['Pays']) 

		 	//&& !empty($_POST['Ville']) && !preg_match('/^[A-Za-z\d ]{1,50}$/', $_POST['Ville']) 

		 	//&& !empty($_POST['Adresse']) && !preg_match('/^[A-Za-z\d ]{5,50}$/', $_POST['Adresse']) 

		 	//&& !empty($_POST['Couchage']) && !preg_match('/^[\d]{1,3}$/', $_POST['Couchage']) 

		 	//&& !empty($_POST['Prix']) && !preg_match('/^[\d]{0,50}$/', $_POST['Prix']) 

		 	//&& !empty($_POST['Date_Debut']) && !preg_match('/^[\d\/ ]{10,50}$/', $_POST['Date_Debut']) 

		 	//&& !empty($_POST['Date_Fin']) && !preg_match('/^[\d\/ ]{10,50}$/', $_POST['Date_Fin']) 

		 	//&& ($_POST['Choix_Type'] == "Logement entier"
		 	 //|| $_POST['Choix_Type'] == "Chambre privé"
		 	 //|| $_POST['Choix_Type'] == "Chambre partager"))
 		{
		    $request = new Annonce();
 			$request->image = $_SESSION['image'];
 			$request->author_id = $_SESSION['user']->id;
 			$request->pays = $_POST['Pays'];
 			$request->ville = $_POST['Ville'];
 			$request->adresse = $_POST['Adresse'];
 			$request->couchage = $_POST['Couchage'];
 			$request->price = $_POST['Prix'];
 			$request->taille = $_POST['Taille'];
 			$request->disponible_start = $_POST['Date_Debut'];
 			$request->disponible_end = $_POST['Date_Fin'];
 			$request->type = $_POST['Choix_Type'];
 			$request->description = $_POST['Description'];
			$annonce = $this->rm->getAnnonceRepo()->createAnnonce($request);

			$view = new View('reussite_annonces.html.twig');


 		}
 		else
 		{
 			View::getError(3);
 		}
	}

	public function AjoutAnnoncesImage(): void
	{
			$uploaddir = 'assets/images/';
			$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
			if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) 
			{
				$_SESSION['image'] = $_FILES['userfile']['name'];
				$view = new View('ajoutannonces.html.twig');

				$view->renderTwig([ 
					
				]);
			}
			else 
			{
			    View::getError(7); // erreur probleme image
			}
	}

	public function SupressionAnnonce(): void
	{
		require_once("App\Defines\Verification.php");

		if(isset($_GET['id']))
		{
			$id = $_GET['id'];
			$this->rm->getAnnonceRepo()->deleted($id);
			header('location: Annonces');
		}
		else
		{
			View::getError(6); // erreur page interdite sans passer par un lien
		}
	}

	public function Reservations(): void
	{
		$othertable = 'annonces';
		$datatoget = 'annonces.author_id';

		if(isset($_SESSION['user']->id))
		{
			$data = $_SESSION['user']->id;
		}
		else
		{
			View::getError(5);
		}

		$view = new View('reservationslistevendeur.html.twig');
		$view->renderTwig([ 
			'reservations' => $this->rm->getReservationRepo()->findByLiaison($datatoget, $data, $othertable)
		]);
	}

}