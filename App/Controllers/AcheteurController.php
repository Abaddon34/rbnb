<?php

namespace App\Controllers;

use App\Models\Reservation; 
use Core\Controller;
use Core\View;
use App\Repositories\RepositoryManager;

class AcheteurController extends Controller
{

	public function Reservations(): void
	{
		
		$datatoget = 'reservor_id';

		if(isset($_SESSION['user']->id))
		{
			$data = $_SESSION['user']->id;
		}
		else
		{
			View::getError(5); // erreur role
		}

		$view = new View('reservationslisteacheteur.html.twig');
		$view->renderTwig([ 
			'reservations' => $this->rm->getReservationRepo()->findBySomething($datatoget, $data)
		]);
		
	}

	public function Reserve(): void
	{
		$datatoget = 'annonce_id';

		if(isset($_GET['id']))
		{
			$data = $_GET['id'];
			$view = new View('reserve.html.twig');
			$view->renderTwig([ 
				'annonce' => $data,
				'reservations' => $this->rm->getReservationRepo()->findBySomething($datatoget, $data)
			]);
		}
		else
		{
			View::getError(5); 
		}
	}

	public function ReserveAction(): void
	{
		if(!empty(!empty($_POST['Date_Debut']))) //&& !preg_match('/^[\d\/ ]{10,50}$/', $_POST['Date_Debut']) && !empty($_POST['Date_Fin']) && !preg_match('/^[\d\/ ]{10,50}$/', $_POST['Date_Fin']))) 
 		{
 			if(isset($_SESSION['user']->id) && isset($_GET['id']))
			{
			    $request = new Reservation();
	 			$request->reservor_id = $_SESSION['user']->id;
	 			$request->annonce_id = $_GET['id'];
	 			$request->reservation_start = $_POST['Date_Debut'];
	 			$request->reservation_end = $_POST['Date_Fin'];
				$reservation = $this->rm->getReservationRepo()->createReservation($request);

				$view = new View('reussite_reservation.html.twig');
			}
			else
			{
				View::getError(5);
			}

 		}
 		else
 		{
 			View::getError(3);
 		}
	}

}